# Notas de Aula #


### Passo a passo ###

* Criar conta no Bitbucket
* Instalar o virtualenv   
* Adicionar ao .bashrc (echo "source virtualenvwrapper.sh" >> ~/.bashrc) 
* Criar Ambiente Virtual (mkvirtualenv nome) após criar deactivate para sair do ambiente e workon nome para entrar
* Instalar o django pip freeze para mostrar a versão atual e pip install -U django==1.7 pip install pillow para instalar o django e o píllow
* Criar uma lista de requerimentos com pip freeze > requerements.txt e instalar com pip install -r requerements.txt
* Verificar versão  python --version  python -c "import django:print django
* Criar o projeto (django-admin.py startproject nome_do_projeto)
* Configurar lista do que o git ignora alterando o arquivo .gitignore
* Criar o Banco de Dados com o sqlite dentro da pasta do projeto usando python manage.py python manage.py migrate testar com python manage.py runserver(ctrl + C para sair do runserver)
* Criar uma aplicação usando python manage.py startapp nome do aplicativo
* Commandos para salvar as configurações no Bitbucket git status (verificar os arquivos adicionados)git add nome do arquivo (adicionar arquivos) e depois git commit -m 'nome do arquivo'(comitar e descrever o que foi comitado) git push -u origin master(para mandar para o Bitbucket)
* Instalar o pycharm
* Criar a pasta templates dentro da pasta tango_with_django_project e nela criar uma pasta rango e criar um index.html nela.
* Ir em settings.py e adicionar as linhas abaixo para depois poder chamar as templates no index.html
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

TEMPLATE_PATH = os.path.join(BASE_DIR, 'templates')
TEMPLATE_DIRS = (
    TEMPLATE_PATH,
)

* Criar uma pasta static dentro da pasta tango_with_django_project e nela criar uma pasta images e adicionar uma imagem nela.
* Ir em settings.py e adicionar as linhas abaixo para depois poder chamar as imagens no index.html

STATIC_URL = '/static/'

STATIC_PATH = os.path.join(BASE_DIR,'static')

STATICFILES_DIRS = (
    STATIC_PATH,
)
* Dar commit no Pycharm usando Ctrl+k
* Criar a pasta media dentro da pasta tango_with_django_project e adicionar uma imagem nela.
* Adicionala no arquivo settings do projeto colocando as seguintes linhas nele

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media') # Absolute path to the media directory

* Criar uma template about.html e adicionala a pagina about atraves do arquivo views.py de modo similar ao index.

* Criar o modelo do banco de dados com category e pages adicionando os comandos abaixo no settings.py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

e no models.py o codigo

from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        return self.name

class Page(models.Model):
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=128)
    url = models.URLField()
    views = models.ImageField(default=0)

    def __unicode__(self):
        return self.name

* Criar um super usuario e migrar o banco de dados com os seguintes comandos no terminal
python manage.py createsuperuser
python manage.py migrate
python manage.py makemigrations rango

* No admin.py adicionar 
from django.contrib import admin
from rango.models import Category, Page

admin.site.register(Category)
admin.site.register(Page)

* Para acessar o admin via navegador usar url http://127.0.0.1:8000/admin/

* Shift+F6 para alterar o nome de uma variavel em todos locais onde ela aparece

* Criar um arquivo populate_rango.py na pasta tango_with_django_project e colocar o commando abaixo , depois no terminal digitar python populate_rango.py

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tango_with_django_project.settings')

import django
django.setup()

from rango.models import Category, Page

def populate():
    python_cat = add_cat('Python')

    add_page(cat=python_cat,
        title="Official Python Tutorial",
        url="http://docs.python.org/2/tutorial/")

    add_page(cat=python_cat,
        title="How to Think like a Computer Scientist",
        url="http://www.greenteapress.com/thinkpython/")

    add_page(cat=python_cat,
        title="Learn Python in 10 Minutes",
        url="http://www.korokithakis.net/tutorials/python/")

    django_cat = add_cat("Django")

    add_page(cat=django_cat,
        title="Official Django Tutorial",
        url="https://docs.djangoproject.com/en/1.5/intro/tutorial01/")

    add_page(cat=django_cat,
        title="Django Rocks",
        url="http://www.djangorocks.com/")

    add_page(cat=django_cat,
        title="How to Tango with Django",
        url="http://www.tangowithdjango.com/")

    frame_cat = add_cat("Other Frameworks")

    add_page(cat=frame_cat,
        title="Bottle",
        url="http://bottlepy.org/docs/dev/")

    add_page(cat=frame_cat,
        title="Flask",
        url="http://flask.pocoo.org")

    # Print out what we have added to the user.
    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print "- {0} - {1}".format(str(c), str(p))

def add_page(cat, title, url, views=0):
    p = Page.objects.get_or_create(category=cat, title=title)[0]
    p.url=url
    p.views=views
    p.save()
    return p

def add_cat(name):
    c = Category.objects.get_or_create(name=name)[0]
    return c

# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    populate()

### Recriar o migrations ###

* No projeto apagar os arquivos da pasta migrations deixando apanas os _init_
* Depois no terminal usar os comandos python manage.py migrate, python manage.py makemigrations e python manage.py createsuperuser
* Por fim python populate_rango.py
* Criar o add_page para isso é preciso criar url criar link criar form criar template criar model criar view
* No admin incluir o UserProfile
* No pycharm ctrl+alt+i para identar