#coding: utf-8

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from models import Category, Page
from forms import CategoryForm, PageForm, UserForm, UserProfileForm
from django.contrib.auth import logout

def index(request):
    context_dict = {}
    category_list = Category.objects.order_by('-likes')[:5]
    context_dict['categories'] = category_list

    page_list = Page.objects.order_by('-views')[:5]
    context_dict['pages'] = page_list

    #print(context_dict)
    return render(request, 'rango/index.html', context_dict)


def about(request):
    context_dict = {'mensagem_em_negrito':'Esta é a página About'}
    return render(request, 'rango/about.html', context_dict)

def category(request, category_name_slug):
    context_dict = {}
    try:
        category = Category.objects.get(slug=category_name_slug)
        context_dict['category_name'] = category.name
        pages = Page.objects.filter(category=category).order_by('-views')
        context_dict['pages'] = pages
        context_dict['category'] = category
    except Category.DoesNotExist:
        pass

    #context_dict['category_name'] = category_name_slug
    #context_dict['category'] = category_name_slug
    #print(category_name_slug)
    #return(HttpResponse(category_name_slug))
    return render(request, 'rango/category.html', context_dict)

@login_required
def add_category(request):
    context_dict = {}
    if request.method != "POST":
        form = CategoryForm()
    else:
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return index(request)

    context_dict['form'] = form
    return render(request, 'rango/add_category.html', context_dict)

@login_required
def add_page(request, category_name_slug):
    try:
        cat = Category.objects.get(slug=category_name_slug)
    except Category.DoesNotExist:
        cat = None

    if request.method != 'POST':
        form = PageForm()
    else:
        form = PageForm(request.POST)
        if form.is_valid():
            if cat:
                page = form.save(commit=False)
                page.category = cat
                page.views = 0
                page.save()
                return category(request, category_name_slug)
        else:
            print form.errors

    context_dict = {'form':form, 'category': cat}
    return render(request, 'rango/add_page.html', context_dict)

def register(request):
    context_dict = {}
    print(request.POST)

    registered = False
    if request.method != "POST":
        user_form = UserForm()
        profile_form = UserProfileForm()
    else:
        user_form = UserForm(request.POST)
        profile_form = UserProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            profile.save()
            registered = True

    context_dict['registered'] = registered
    context_dict['user_form'] = user_form
    context_dict['profile_form'] = profile_form
    return render(request, 'rango/register.html', context_dict)

def user_login(request):
    context_dict = {}
    if request.method != "POST":
        return render(request, 'rango/login.html', context_dict)
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/rango/')
            else:
                return HttpResponse("Sua conta está desabilitada.")
        else:
            erro = "Detalhes de login inválidos: Usuário: [{0}], Senha:[{1}]".format(username, password)
            print(erro)
            return HttpResponse(erro)

@login_required
def restricted(request):
    return render(request, 'rango/restricted.html', {})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/rango/')

def track_url(request):
    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id=page_id)
                page.views += 1
                page.save()
                return redirect(page.url)
            except:
                pass
    return redirect('/rango/')

# def add_category(request):
#     context_dict = {}
#
#     return render(request, 'rango/add_category.html', context_dict)